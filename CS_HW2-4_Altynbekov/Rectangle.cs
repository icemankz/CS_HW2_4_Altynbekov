﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CS_HW2_4_Altynbekov
{
    public class Rectangle : SteelPlate
    {
        public int Length { get; set; }
        public int Width { get; set; }
#region Constructors
        public Rectangle(int thickness, int density, int length, int width)
            : base(thickness, density)
        {
            Length = length;
            Width = width;
        }

        public Rectangle()
        {
            // TODO: Complete member initialization
        }
#endregion

        public override void GetInfo()
        {
            Random rand1 = new Random();
            Length = rand1.Next(100, 1000);
            Thread.Sleep(20);
            Random rand2 = new Random();
            Width = rand2.Next(100, 1000);
            Thread.Sleep(20);
            Console.WriteLine("Прямоугольник длиной {0} мм, и шириной {1} мм", Length, Width);
            GetArea();
            GetWeight();
        }

        public override void GetArea()
        {
            rectangleArea = Length * Width;
            Console.WriteLine("Площадь прямогольного листа равна: " + rectangleArea.ToString("0.00") + " мм2.");
        }

        public override void GetWeight()
        {
            rectangleWeight = (Length * Width * Thickness) * 1e-9 * Density;
            Console.WriteLine("Вес прямогольного листа равен: " + rectangleWeight.ToString("0.00") + " kg");
        }
    }
}
