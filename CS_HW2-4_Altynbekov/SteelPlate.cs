﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CS_HW2_4_Altynbekov
{
    public class SteelPlate
    {
        public int Thickness = 5;
        public int Density = 7800;
        public double triangleArea;
        public double rectangleArea;
        public double squareArea;
        public double triangleWeight;
        public double rectangleWeight;
        public double squareWeight;

#region Constructors
        public SteelPlate() { }

        public SteelPlate(int thickness,
            int density)
        {
            Thickness = thickness;
            Density = density;
        }
#endregion

        public virtual void GetInfo()
        {
            
        }

        public virtual void GetArea()
        {
            
        }

        public virtual void GetWeight()
        {
            
        }
      
    }
}
