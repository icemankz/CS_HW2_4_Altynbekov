﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CS_HW2_4_Altynbekov
{
    class Program
    {
        static void Main(string[] args)
        {         
            List<SteelPlate> steelPlates = new List<SteelPlate>()
            {
            new Triangle(),
            new Triangle(),
            new Triangle(),
            new Triangle(),
            new Triangle(),
            new Rectangle(),
            new Rectangle(),
            new Rectangle(),
            new Rectangle(),
            new Rectangle(),
            new Rectangle(),
            new Rectangle(),
            new Rectangle(),
            new Square(),
            new Square(),
            new Square()
            };
      
            foreach (var plate in steelPlates)
            {
                plate.GetInfo();               
            }

            double triangleAreaSum = steelPlates.Sum(n => n.triangleArea);
            double rectangleAreaSum = steelPlates.Sum(n => n.rectangleArea);
            double squareAreaSum = steelPlates.Sum(n => n.squareArea);
            double totalAreaSum = triangleAreaSum + rectangleAreaSum + squareAreaSum;
            Console.WriteLine("Суммарная площадь листов: " + (totalAreaSum * 1e-6).ToString("0.00") + " м2.");

            double triangleWeightSum = steelPlates.Sum(n => n.triangleWeight);
            double rectangleWeightSum = steelPlates.Sum(n => n.rectangleWeight);
            double squareWeightSum = steelPlates.Sum(n => n.squareWeight);
            double totalWeightSum = triangleWeightSum + rectangleWeightSum + squareWeightSum;
            Console.WriteLine("Суммарный вес листов: " + totalWeightSum.ToString("0.00") + " kg.");
        }     
    }
}
