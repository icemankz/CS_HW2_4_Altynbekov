﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CS_HW2_4_Altynbekov
{
    public class Square : SteelPlate
    {
        public int LengthWidth { get; set; }
#region Constructors
        public Square(int thickness, int density, int lengthWidth)
            : base(thickness, density)
        {
            LengthWidth = lengthWidth;
        }

        public Square()
        {
            // TODO: Complete member initialization
        }
#endregion

        public override void GetInfo()
        {
            Random rand = new Random();
            LengthWidth = rand.Next(100, 1000);
            Thread.Sleep(20);
            Console.WriteLine("Квадрат со сторонами {0} мм каждая ", LengthWidth);
            GetArea();
            GetWeight();
        }

        public override void GetArea()
        {
            squareArea = LengthWidth * LengthWidth;
            Console.WriteLine("Площадь прямогольного листа равна: " + squareArea.ToString("0.00") + " мм2.");
        }

        public override void GetWeight()
        {
            squareWeight = (LengthWidth * LengthWidth * Thickness) * 1e-9 * Density;
            Console.WriteLine("Вес прямогольного листа равен: " + squareWeight.ToString("0.00") + " kg");
        }
    }
}
