﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CS_HW2_4_Altynbekov
{
    public class Triangle : SteelPlate
    {
        public int Cathetus1 { get; set; }
        public int Cathetus2 { get; set; }
#region Constructors
        public Triangle(int thickness, int density, int cathetus1, int cathetus2)
            : base(thickness, density)
        {
            Cathetus1 = cathetus1;
            Cathetus2 = cathetus2;
        }

        public Triangle()
        {
            // TODO: Complete member initialization
        }
#endregion
        public override void GetInfo()
        {
            Random rand1 = new Random();
            Cathetus1 = rand1.Next(100, 1000);
            Thread.Sleep(20);
            Random rand2 = new Random();
            Cathetus2 = rand2.Next(100, 1000);
            Thread.Sleep(20);
            Console.WriteLine("Треугольный лист с катетом 1 - {0} мм., и катетом 2 - {1} мм.", Cathetus1, Cathetus2);
            GetArea();
            GetWeight();       
        }
      
        public override void GetArea() 
        {
            triangleArea = Cathetus1 * Cathetus2 / 2;
            Console.WriteLine("Площадь треугольного листа равна: " + triangleArea.ToString("0.00") + " мм2.");
        }

        public override void GetWeight()
        {
            triangleWeight = (Cathetus1 * Cathetus2 / 2 * Thickness) * 1e-9 * Density;
            Console.WriteLine("Вес треугольного листа равен: " + triangleWeight.ToString("0.00") + " kg");
        }
    }
}
